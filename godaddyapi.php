<?php
require_once __DIR__ . '/controller.php';

class GodaddyAPI extends Controller implements Settings
{
	public $header;
	public $pricelimit;
	public $top;
	public $domaincache;
	public $domains;
	public $maximum;
	public $results;
	public $extension;
	
	public function __construct($names=null,$max=40,$pricelimit=12,$top=100,$extension='.com'){
		$this->domains = $this->results = [];
		$speed = FALSE;
		$test_key = '2s7YfKEHQU_2yq9DZNhsMn2PG3PQbHSv8';
		$test_secret = '2yqCGyH6JnC4AdVKmZ7ZWC';
		$key = '9ZpU9FFR63i_7Ume4MVnnwoeF85bK5F2Fb';
		$secret = '7Umi8uNJiB2MHcgB8MgWNM';
		$this->header = array('Content-Type: application/json', 'Accept: application/json', 'Authorization: sso-key '.$key.':'.$secret);
		$this->maximum = $max; 				// max per call
		$this->pricelimit = $pricelimit*1000000; 	// price limit
		$this->top = $top;					// top results to analyze
		$this->extension = $extension;
		$this->domaincache = 'domain_cache';
		$query = ($speed === TRUE)? 'FAST' : 'FULL';		
		if($names!==null){
			$this->names = $names;
			self::getDomains($query);
			//self::writeDomains();
		}
		return true;
	}
	
	public function getTlds(){
		$url = self::setURL('tlds',null);
		$results = self::cache(TLDDIR.'tld'.EXT,null,'GET',$url,$this->header);
		$results = array_column($results,'name');
		$results = preg_filter('/^/','.',$results);
		return $results;
	}
	
	public function getDomains($query){
		$delim = count($this->names)/$this->maximum;	// number of times we'll have to run through;
		$url = self::setURL('domains',$query);
		$domains = [];
		$count = 0;
		for($i=0;$i<$delim;$i++){
			$start = $i*$this->maximum;
			$end = $start+$this->maximum-1;
			$body = array_slice($this->names,$start,$end);
			$domains = preg_filter('/$/',$this->extension,$body);
			$this->results = $this->domains = array_flip($body);
			$domains = json_encode($domains);
			$result = self::getAPI('POST',$url,$this->header,$domains);
			$result = json_decode($result);
			$count = $count+self::validateDomains($result);
			if($count >= $this->top){
				break;
			}
		}
		return true;
	}
	
	public function validateDomains($object){
		$count = 0;
		if(isset($object->domains)) {
			foreach($object->domains as $item){
				$name = str_replace($this->extension,'',$item->domain);
				$link = affiliate.url.$item->domain;
				$package = ['name'=>$name,'domain'=>$item->domain,'available'=>$item->available,'link'=>$link];
				$package['price'] = isset($item->price)? $item->price/1000000: null;
				if(!in_array($name,array_column($this->domains,'name'))){
					$this->results[$name] = $package;
				}
				if($item->available!==false&&$item->price<=$this->pricelimit){
					if(array_key_exists($name,$this->domains)){
						$this->domains[$name] = $package;
						$count++;
					}
				}
			}
		}
		return $count;
	}
	
	public function writeDomains(){
		//self::function cache(ROOTDIR.domainfile.EXT,json_encode($this->domains));
		@file_put_contents(CACHEDIR.domainfile.EXT,json_encode($this->domains), FILE_APPEND);
		return true;
	}
}


?>