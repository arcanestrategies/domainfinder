<?php
require_once __DIR__ . '/controller.php';
require_once __DIR__ . '/wordsapi.php';
	
class Words extends Controller implements Settings
{
	private $database;
	private $expiration;
	private $link;
	public $properties;
	public $query;
	
	public function __construct($db=platform,$expiration='0000-00-00 00:00:00'){
		$database = new db($db);
		$this->database = $database;
		$this->expiration = $expiration;
		$this->link = new mysqli($database->host, $database->un, $database->pw, $database->db);
		$this->api = new WordsAPI();
		return true;
	}
	
	public function setVars(&$query,$properties=NULL){
		$this->query = $query;
		$this->properties = $properties;
		/*$query['consecConsenants'] = $this->consecConsenantsMax;
		$query['consecVowels'] = $this->consecVowelsMax;*/
	}
	
	/**
		gets words which match pattern
	**/
	public function getMatches($algorithm){
		$result = [];
		$queries = $algorithm->query;
		$select = 'SELECT `properties`.`word`';
		$where = $join = '';
		//$queries['lettersMin'] = $algorithm->wordCountMin; 	// NOTE: The minimum letters of a returned part should always be the minimum letters of a requested part...
		//$queries['lettersMax'] = $algorithm->wordCountMax;	// NOTE: This is where it gets confusing... do we want only the PART or a whole word?
		foreach($queries as $key=>$value){
			if(false !== strpos(strtolower($key),'max')||false !== strpos(strtolower($key),'min')){
				$conjunction = ' AND ';
				$operator = strtolower(substr($key, -3));
				$key = strtolower(substr($key,0,-3));
				switch($operator){
					case 'min':
						$op = '>=';
						break;
					case 'max':
						$op = '<=';
						break;
				}
				$where .= $conjunction.'`properties`.`'.$key.'`'.$op.'"'.$value.'"';
			}
			$kmode = $key.'Wmode';
			$conjunction = (isset($algorithm->$kmode)&&strictThreshold<$algorithm->$kmode)? ' AND ' : ' OR ' ; // NOTE: We only do a strict AND search if the property's value is higher than the strictThreshold
			if($key=='letterPattern'&&regexfilter==true){
				$conjunction = ' AND ';
				switch($key){
					default:
						$key = 'word';
						break;						
				}
				$each = explode('?',$value);
				foreach($each as $v){
					if(strpos($v,'[')!=false||strpos($v,'{')!=false){
						$binary = ($v[0]=='!')? 'NOT ' : '';
						$v = str_replace('!','',$v);
						$v = addslashes($v);
						if(false==strpos($v,'[')){
							$v = '([a-z])'.$v;
							//$v = str_replace('}',',}',$v);
							$st = 0;
						} else {
							$st = strpos($v,'[');
						}
						$en = (($en = strpos($v,'}'))!==false)? $en : strpos($v,']');
						$en = $en-$st;
						$v = substr($v,$st,$en+1);
						$where .= $conjunction.'`properties`.`'.$key.'` '.$binary.'REGEXP "'.$v.'"';
					}
				}
			}
			if($key=='hasDetails'){
				$value = explode(',',$value);
				$i=0;
				if(inclause==false){
					$join2 = '';
				}
				foreach($value as $property){
					if(in_array($property,disableDetailInSearch)){
						continue;
					}
					if(lowercasetables==true){
						$property = strtolower($property);
					}
					$propertyeach = $property.'Weach';
					if(!empty($algorithm->$propertyeach)){
						$conjunction = (isset($algorithm->$kmode)&&strictThreshold<$algorithm->$kmode)? ' AND ' : ' OR ' ;
						if($i==0&&inclause==true){
							$conjunction = ' AND (';
						}
						$join .= ' LEFT JOIN `'.$property.'` ON (`properties`.`id`=`'.$property.'`.`id`';
						if(inclause==true){
							$where .= $conjunction.'`'.$property.'`.`'.$property.'` IN ("'.implode('","',array_keys($algorithm->$propertyeach)).'")';
							if(count(array_keys($algorithm->$propertyeach))>0){
								$join .= ' AND `'.$property.'`.`'.$property.'` IN ("'.implode('","',array_keys($algorithm->$propertyeach)).'"))';
							} else {
								$join .= ')';
							}
						} else {
							if($i>0){
								$join2 .= ' RIGHT JOIN (';
							}
							$h = 0;
							foreach(array_keys($algorithm->$propertyeach) as $x){
								if($h==0){
									$xyz = 'SELECT "'.$x.'" AS "y"';
								} else {
									$xyz = ' UNION ALL SELECT "'.$x.'"';
								}
								$join2 .= $xyz;
								$h++;
							}
							if($i>0){
								$join2 .= ') AS `'.$property.'x` ON `'.$property.'x`.`y`=`'.$property.'`.`'.$property.'`';
							}
							$join2 .= ') AS `'.$property.'x` ON `'.$property.'x`.`y`=`'.$property.'`.`'.$property.'`';
						}
						$i++;
					}
				}
				if($i>0&&inclause==true){
					$where .= ')';
				}
			}
		}
		if(inclause==false){
			$innerjoinopen = ' INNER JOIN (';
			$innerjoinclose = ') AS `g` ON `properties`.`id`=`g`.`id`';
			$join=$join.$innerjoinopen.$join2.$innerjoinclose;
		}
		$where = preg_replace("/^(\w+\s)/", " WHERE ", ltrim($where));
		$sql = $select.' FROM `properties`'.$join.$where.' GROUP BY `properties`.`id`;';
		try{
			$query = $this->link->query($sql);
			if($query!==false&&$query->num_rows > 0) {
				// REFACTOR: Must be able to merge in the multiple values as a single property array for each object
				while($row = $query->fetch_assoc()) {
					$result[] = $row['word'];
				}
			}
		} catch (Exception $e) {
			//$result = 'Caught exception: '.  $e->getMessage(). "\n";
			$result = false;
		}
		return $result;
	}
	
	public function getTables(){
		$result = [];
		$sql = 'SELECT table_name FROM information_schema.TABLES where TABLE_SCHEMA="'.$this->database->db.'"';
		try{
			$query = $this->link->query($sql);
			if($query!==false&&$query->num_rows > 0) {
				while($row = $query->fetch_assoc()) {
					$result[] = reset($row);
				}
			} else {
				$result = false;
			}
		} catch (Exception $e) {
			//$result = 'Caught exception: '.  $e->getMessage(). "\n";
			$result = false;
		}
		return $result;
	}
	
	public function explodeWord(&$row){
		if(is_object($row)){
			foreach($row as $key=>$val){
				if(isset($this->properties[$key])&&$this->properties[$key]==2&&(in_array($key,wordsAPIhasDetails)||in_array($key,wordshasDetails))){
					if(!is_array($val)){
						$value = !empty($val)? explode(',',$val) : NULL;
					}
					$row->$key = $value;
				}
			}
		}
	}

	public function getWord($word){
		$tables = self::getTables();
		$select = 'SELECT `properties`.*';
		$where = ' WHERE `properties`.`word`="'.$word.'"';
		$join = '';
		foreach(array_keys($this->properties) as $property){
			if($tables!==false&&!in_array($property,$tables)){
				continue;
			}
			$select .= ', GROUP_CONCAT(DISTINCT(`'.$property.'`.`'.$property.'`)) as `'.$property.'`';
			$join .= ' LEFT JOIN `'.$property.'` ON `properties`.`id`=`'.$property.'`.`id`';
		}
		$sql = $select.' FROM `properties`'.$join.$where.' GROUP BY `properties`.`id` LIMIT 1';
		try{
			$query = $this->link->query($sql);
			if($query!==false&&$query->num_rows > 0) {
				while($row = $query->fetch_assoc()) {
					$result = (object)$row;
					self::explodeWord($result);
				}
			} else {
				$result = false;
			}
		} catch (Exception $e) {
			//'Caught exception: '.  $e->getMessage(). "\n";
			$result = false;
		}
		return $result;
	}

	public function getFiles(){
		$files = array_diff(scandir(WORDIR), array('..', '.', '.text'));
		return $files;
	}	
	
	public function getWordFile($string){
		$results = self::cache(WORDIR.$string.EXT);
		if($results==false){
			$results = $this->api->getWord($string);
		}
		return $results;
	}
	
	public function setWords($word=NULL){
		if($word==NULL){
			$word = self::getFiles();
		}
		if(is_array($word)){
			foreach($word as $w){
				self::setWord(str_replace('.txt','',$w));
			}
		} else {
			self::setWord($word);
		}
		return true;
	}
	
	public function setProperty($var,$table,$id,$unique=false){
		$col = $collection = $table;
		$sql2 = '';
		if(is_array($table)){
			$col = $table[1];
			if(isset($table[2])){
				$col .= '_'.$table[2];
			}
			$table = $table[0];
			if(!is_object($var)){
				$sql2 = ' ON DUPLICATE KEY UPDATE `id`="'.$id.'",`'.$col.'`="'.$this->link->real_escape_string($var).'"';
			}
		}
		if(self::is_assoc($var)==true){
			foreach($var as $prop=>$v){
				$collection = ($prop=='count'||is_numeric($v))? ['properties',$table] : $table;
				self::setProperty($v,$collection,$id);
			}			
		} else if(is_array($var)||is_object($var)){
			foreach($var as $v){
				self::setProperty($v,$collection,$id);
			}
		} else {
			if(!empty($var)){
				$ignore = empty($sql2)? 'IGNORE ' : '';
				$sql = 'INSERT '.$ignore.'INTO `'.$table.'` (`id`,`'.$col.'`) VALUES ("'.$id.'","'.$this->link->real_escape_string($var).'")';
				$sql.=$sql2;
				try{
					$result = $this->link->query($sql);
				} catch (Exception $e) {
					$result = 'Caught exception: '.  $e->getMessage(). "\n";
				}
			}
		}
		return true;
	}
	
	public function setWord($file,$flag=false){
		$result = false;
		if($flag==true){
			$sql_0 = 'INSERT INTO `properties` (`word`,`created_at`,`modified_at`) VALUES ("'.$this->link->real_escape_string($file).'",NULL,NULL) ON DUPLICATE KEY UPDATE `word`="'.$this->link->real_escape_string($file).'",`modified_at`=NULL';
			try{
				$result = $this->link->query($sql_0);
			} catch (Exception $e) {
				//$result = 'Caught exception: '.  $e->getMessage(). "\n";
				$result = false;
			}
		}
		$word = self::getWord($file);
		if($word==false&&$flag==false){
			self::setWord($file,true);
		}
		/** This uses expiration for update
		if($word!==false&&$flag==false&&strtotime($word->modified_at)>=strtotime($this->expiration)){
			return false;
		}
		**/
		$properties = self::getWordFile($file);
		$sql_1 = 'INSERT INTO `properties`';
		$keys = '(`created_at`,`modified_at`';
		$values = 'VALUES (NULL,NULL';
		$ondupe = 'ON DUPLICATE KEY UPDATE `modified_at`=NULL';
		if($word!==false&&isset($word->id)&&$properties!==false){
			foreach($properties as $table=>$value){
				if($table=="created_at"||$table=="modified_at"){
					continue;
				}
				if(is_array($value)||is_object($value)){
					self::setProperty($value,$table,$word->id);
				} else {
					// REFACTOR: In theory we can just use setProperty on these for cleaner code but this creates a single SQL statement which should speed things up.
					if(!empty($value)&&$value!=="NULL"){
						$keys .= ',`'.$table.'`';
						$values .= ',"'.$this->link->real_escape_string($value).'"';
						$ondupe .= ',`'.$table.'`="'.$this->link->real_escape_string($value).'"';
					}
				}
			}
			$sql_1 .= $keys.') '.$values.') '.$ondupe;
			try{
				$result = $this->link->query($sql_1);
			} catch (Exception $e) {
				//$result = 'Caught exception: '.  $e->getMessage(). "\n";
				$result = false;
			}
		}
		return $result;
	}
}


?>
