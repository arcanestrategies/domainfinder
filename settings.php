<?php
error_reporting(E_ERROR);
$host = '127.0.0.1';
$un = 'root';
define('pw','400c87G4o8SCdZb');
define('logger','/var/www/html/domainfinder/log/api.log');
define('affiliate','http://www.dpbolvw.net/click-8891574-11429065?url=');				// Affiliate tracking code for results
define('url','https://www.namecheap.com/domains/registration/results.aspx?domain=');	// Search result URL
define('inclause',TRUE);				// Enable IN clause on MySQL or use a select/join (if disabled)
define('strictThreshold',0.51);			// Threshold between 0 and 1 at which point we determine a property is consistent enough to determine a boundary.  Recommended above 0.5.
define('regexfilter',TRUE);			// Enable/Disable use of regex.
define('specialchars',FALSE);			// Enable/Disable regex which disallows special characters
define('doublecharacterpairs',TRUE);	// Enable/Disable regex which allows 2 consecutive double characters
define('standardpronunciations',TRUE);	// Enable/Disable nonstandard pronunciation characters in regex
define('lowercasetables',FALSE);
define('includeparts',true);			// Enable/Disable inclusion of a word's parts in ranking
define('multiplier',1000000);
define('percentile',5);			// Return top x percentile of results
define('limiter',80);			// Return top X number of results		
define('duration',365);			// Length of caching (days)... this is sort of irrelevant with the database
define('buildWordsTimeout',38);	// Amount of seconds we'll allow the words building stage to run for, before we force it to move on
define('strictCount',8000);		// Amount of results from the initial query at which we re-run with a stricter pattern (we check the local database as well)
define('WORDIR', __DIR__ . DIRECTORY_SEPARATOR .'words'. DIRECTORY_SEPARATOR);	// Cache directory for word files
define('TLDDIR', __DIR__ . DIRECTORY_SEPARATOR .'tlds'. DIRECTORY_SEPARATOR);	// Cache directory for TLDs
define('CACHEDIR', __DIR__ . DIRECTORY_SEPARATOR);								// Root cahce directory
define('domainfile', 'domains');	// Filename for domain result storage			
define('apilimit', 50000);		// Default API limitation (maximum # of API calls made per API per run)
define('pagination', 100);		// Default number of results per page in API call response
define('EXT','.txt');			// File extension for cached files
define('platform','live');		// To switch to live, change platform to live
define('lettersMin',4);
define('lettersMax',5);
define('syllablesMin',1);
define('syllablesMax',2); // Per word syllables. With 2 words, that means max 4.
define('consecConsenantsMax',3);
define('consecVowelsMax',2);
define('wordCountMin',4);
define('wordCountMax',8);
define('frequencyMin',2.75);
define('frequencyMax',5.75);
define('brands',[['face','book'],'twitter',['insta','gram'],['snap','chat'],'google','uber','yahoo',['you','tube'],['net','flix'],['spot','ify']]);	// REFACTOR:  Use a flat array, use library to determine root words from each string, then we could easily count consenants and vowels for a word, wordcount, syllable count, etc... and it'd be more scalable
// REFACTOR: This is manually copied from the library in the Syllables vendor file.  We could probably just import it from that vendor file
define('parts',['cia','tia','giu','iou','sia','gue','ely','jua','uia','eau','riet','dien','iu','io','eo','ii','lien','coad','coag','coal','coax','oa','ou','iell','dea','uen']);
define('prefixes',['un','fore','ware','sub','pre','pro','dis','anti','ante','hyper','afore','agri','intra','infra','inter','over','semi','dia','micro','mega','kilo','mc','pico','nano','macro']);
define('suffixes',['cius','eous','cious','ly','less','ful','ers','ness','cians','ments','ettes','villes','s','tion','tioned','ology','ologist','onomy','onomist','ism','asm','thm','dnt','uity','eings','gean']);
define('bleep',['pube','racist','victim','twat','jihad','pubes','orgasm','vagina','penis','cock','dick','cunt','fag','gay','homo','bitch','shit','fuck','fucker','ass','bastard','nigger','spook','rape','slut','molest','hezbollah','isis',"al-qa'ida",'al-qaeda','al-qaida','al qaeda','al qaida','qaeda','coon']);	// words to ignore... warning, this is some graphic language
define('rangevariables',['letters','sounds','syllables','frequency','wordCount','wordSyllables','consecConsenants','consecVowels']); // Algorithm variables which may be passed as mins/maxes
define('wordsAPIKey',"ATKRZg2tC6mshvrds4tp0qSJK5OTp10BlegjsnRbZu7yA4uteh");
define('wordsAPIParams',['letterPattern','letters','pronunciationPattern','sounds','syllables','frequency','partOfSpeech','hasDetails']);
define('wordsAPIhasDetails',['synonyms','antonyms','typeOf','hasTypes','partOf','hasParts','instanceOf','hasInstances','similarTo','also','entails','memberOf','hasMembers','substanceOf','hasSubstances','inCategory','hasCategories','usageOf','hasUsages','inRegion','regionOf','pertainsTo']);	// partOfSpeech might be a good thing to consider.  removed "'definition'=>1", too liquid
// 'definition','examples'
define('disableDetailInSearch',['definition','examples']);
define('wordshasDetails',['parts','syllableparts','pronunciation']);
define('wordProperties',['wordCount','wordSyllables','consecConsenants','consecVowels','startChar','endChar','startPos','endPos','posSwitch']);

interface Settings
{
	
}

class db {
	public $host;
	public $db;
	public $un;
	public $pw;

	public function __construct($platform='live'){
			if($platform == 'dev' || $platform == 'local'){
					$this->host = '127.0.0.1';
					$this->db = 'words';
					$this->un = 'root';
					$this->pw = '';
			} else {
					$this->host = '127.0.0.1';
					$this->db = 'words';
					$this->un = 'root';
					$this->pw = pw;
			}
			return true;
	}
}

?>
