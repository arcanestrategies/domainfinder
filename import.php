<?php
require_once __DIR__ . '/algorithm.php';
// Import txt files to MySQL
if (defined('STDIN')) {
        if (!empty($argv[1])) {
			$word = $argv[1];
		}
        if (!empty($argv[2])) {
            $db = $argv[2];
        }
} else {
        if (!empty($_GET) && !empty($_GET['word'])) {
			$word = $_GET['word'];
		}
        if (!empty($_GET) && !empty($_GET['db'])) {
            $db = $_GET['db'];
        }
}
$full = (!isset($word)||$word=='?')? TRUE : FALSE;
$db = isset($db)? $db : NULL;

date_default_timezone_set('america/new_york');
$date = date('Y-m-d H:i:s');
$expiration = date('Y-m-d H:i:s', strtotime($date." -".duration." days"));

$algorithm = new Algorithm();
$wordobj = new Words($db);
$wordobj->properties = $algorithm->wordsProperties;

if($full==TRUE){
	print('importing to '.$db.' everything newer than '.$expiration."\r\n");
	$wordobj->setWords();
} else {
	print('importing '.$word.' to '.$db."\r\n");
	$wordobj->setWords($word);
}

?>