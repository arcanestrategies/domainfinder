<?php
require_once __DIR__ . '/settings.php';
require_once __DIR__ . '/Syllables.php';
/**
	1) Controller accepts 3 parameters: The limit of results per API call, page of results to display, extension for domain like .com or .net
**/

class Controller
{
	public $limit=pagination;				// Results per page.  Override on the class level
	public $page=1;  						// Pagination for results.  Default is 1.  Foreach loop if count is higher than 100?
	public $expiry = 60*60*24*duration;		// Cache duration
	public $callcounter = 0;				// Iterator starts at 0
	public $limitation = apilimit;			// API limitation: Maximum # of API calls made per API per run
	
	public static function writeLog($method,$url,$response,$post=null){
		$timestamp = date('Y-m-d H:i:s');
		$file = logger;
		if(!file_exists($file)){
			touch($file);
		}
		if(file_exists($file)){
			if(false !== ($fp = fopen($file, 'a'))){
				$str = "\n".$timestamp;
				$str .= "\n".$method.':'.$url;
				if(isset($post)){
					$str .= "\n".json_encode($post);
				}
				$str .= "\n".json_encode($response);
				$written = fwrite($fp, $str);
				fclose($fp);
				if($written !== false){
					return true;
				}
			}
		}
		return false;
	}
	
	public function cache($file,$results=null,$method=null,$url=null,$header=null){
		if(file_exists($file) && (filemtime($file) > (time() - $this->expiry)) && $results==null){
			$results = json_decode(@file_get_contents($file));
		} else if($results!==null){
			@file_put_contents($file,json_encode($results));
		} else if($method!=null&&$url!=null&&$header!=null&&$results==null){
			//print('ACTION: '.$method.' '.$url."\r\n");
			$results = json_decode(self::getAPI($method,$url,$header));
			@file_put_contents($file,json_encode($results));
		} else {
			$results = (object)[];
			self::writeLog('ERROR','none','ERROR: Data NULL');
		}
		if($results!==null && $results !== false){
			return $results;
		}
		return false;
	}
	
	public function str_split_unicode($str, $l = 0) {
		if ($l > 0) {
			$ret = array();
			$len = mb_strlen($str, "UTF-8");
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, "UTF-8");
			}
			return $ret;
		}
		return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
	}
	
	public function setStatus($array,$status=1){
		$results = [];
		foreach($array as $key => $val){  // API query properties. "2" = enabled, "1" = enabled w/ mins and maxes.
			if(is_numeric($key)&&!is_numeric($val)){	// If the key is numeric, we know that it takes the default status
				$st = in_array($val,rangevariables)? $status : 2;
				$results[$val] = $st;
			} else {
				$results[$key] = $val;
			}
		}
		return $results;
	}
	
	public function is_assoc($arr){
		$arr = is_array($arr)? $arr : (array)$arr;
		return array_keys($arr) !== range(0, count($arr) - 1);
	}
	
	/**
		is_multi_array checks to see if an array is a multi-dimensional array.
	**/
	public function is_multi_array( $arr ) {
		rsort( $arr );
		return isset( $arr[0] ) && is_array( $arr[0] );
	}

	/**
		array_flatten converts a multi-dimensional array into a single array
	**/
	public function array_flatten($array) {
	  if (!is_array($array)) return FALSE;
	  $result = array();
	  foreach ($array as $key => $value) {
		if (is_array($value))
		  $result = array_merge($result,self::array_flatten($value));
		else $result[$key] = $value;
	  }
	  return $result;
	}
	
	public function explodeString(&$results){
		if(is_string($results)){
			$results = explode(',',$results.",");
			array_filter($results);
		}
	}
	
	public function permutatorString($array){
		$results = [];
		foreach($array as $arg){
			if(!in_array($arg,$results)){
				$results[] = $arg;
			}
			foreach($array as $arg2){
				if($arg!==$arg2){
					if(!in_array($arg.$arg2,$results)){
						$results[] = $arg.$arg2;
					}
				}
			}
		}
		return $results;
	}

	public function permutatorArray($array){
		$results = [];
		foreach($array as $arg){
			foreach($array as $arg2){
				if($arg!==$arg2){
					$results[] = [$arg,$arg2];
				}
			}
		}
		return $results;
	}
	
	public function speechSound($string){
		preg_match_all('/[aeiouy]/', $string, $vowels, PREG_OFFSET_CAPTURE);
		preg_match_all('/[b-df-hj-np-tv-xz]/', $string, $consonants, PREG_OFFSET_CAPTURE);
		if(!isset($j)){ $j = 1; }
		if(!isset($g)){ $g = 1; }
		$vows = $cons = 0;
		$starter = $ender = 'consonant';
		foreach($vowels[0] as $vowel){
			if($vowel[1]==0){$starter = 'vowel'; }  // If the position (key 1=position) of first consonant equals the first character of the string (part or word), set startTypePos = consonant'
			if($vowel[1]==(strlen($string)-1)){ $ender = 'vowel'; }	// If the position (key 1=position) equals the last character of the string, set endTypePos = vowel
			$j = ((isset($last_vowel))&&(($vowel[1]-$last_vowel)==1))? $j+1 : 1;
			$last_vowel = $vowel[1];
			$vows = (!isset($vows)||(isset($vows)&&($j>$vows)))? $j : $vows;
		}
		foreach($consonants[0] as $consonant){
			if($consonant[1]==0){ $starter = 'consonant';}
			if($consonant[1]==(strlen($string)-1)){ $ender = 'consonant'; }
			$g = ((isset($last_consonant))&&(($consonant[1]-$last_consonant)==1))? $g+1 : 1;
			$last_consonant = $consonant[1];
			$cons = (!isset($cons)||(isset($cons)&&($g>$cons)))? $g : $cons;
		}
		return (object)['starter'=>$starter,'ender'=>$ender,'vows'=>$vows,'cons'=>$cons];
	}

	public function findFile($file,$results=[]){
		if(is_array($file)||is_object($file)){
			foreach($file as $ff){
				if(($result = self::findFile($ff,$results))!==false){
					$results = array_merge($results,$result);
				}
			}
		} else {
			$temp = glob(WORDIR.$file.'*'.EXT);
			//$temp = glob(WORDIR.$file.EXT);
			if(!empty($temp)){
				for($i=0;$i<count($temp);$i++){
					$r = self::cache($temp[$i]);
					if(!isset($r->word)xor$r->word==$file){
						continue;
					}
					$tt = str_replace(WORDIR,'',$temp[$i]);
					$tt = str_replace(EXT,'',$tt);
					$leven = levenshtein($file,$tt);
					$results[$leven] = $tt;
				}
			}
		}
		if(!empty($results)){
			return array_unique(ksort($results));
		}
		return false;
	}
	
	public function getAPI($METHOD,$url,$header,$ARG=NULL){
		if(strpos($url,'wordsapiv1') !== false){
			$this->callcounter++;
			if($this->callcounter > $this->limitation){
				self::writeLog($METHOD,$url,'ERROR: limitation reached',$ARG);
				return false;
			}
		}
		$session = curl_init();
		switch ($METHOD)
		{
			case "POST":
				curl_setopt($session, CURLOPT_POST, 1);
				if ($ARG){
					 curl_setopt($session, CURLOPT_POSTFIELDS, $ARG);
				}
				 break;
			case "PUT":
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($session, CURLOPT_CUSTOMREQUEST, "PUT");
				if ($ARG){
					 curl_setopt($session, CURLOPT_POSTFIELDS, $ARG);
				}
				break;
			case "GET":
				curl_setopt($session, CURLOPT_HTTPGET, 1);
		}
		curl_setopt($session, CURLOPT_URL, $url);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_HTTPHEADER, $header);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		//if(ereg("^(https)",$url))
		curl_setopt($session, CURLOPT_SSL_VERIFYPEER,false);
		$response = curl_exec($session);
		curl_close($session);
		self::writeLog($METHOD,$url,$response,$ARG);
		return $response;
	}
	
	public static function require_auth() {
		$AUTH_USER = 'arcane';
		$AUTH_PASS = 'AFs097as5';
		header('Cache-Control: no-cache, must-revalidate, max-age=0');
		$has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
		$is_not_authenticated = (
			!$has_supplied_credentials ||
			$_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
			$_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
		);
		if ($is_not_authenticated) {
			header('HTTP/1.1 401 Authorization Required');
			header('WWW-Authenticate: Basic realm="Access denied"');
			return false;
		}
		return true;
	}
	
	/**
		getPropertyType
		retrieves a list of properties beginning with $start, ending with $end.
	**/
	public function getPropertyType($start,$end,$properties){
		array_filter((array)$properties, function($v,$start,$end){ // Don't think I can pass 3 vars to a callback like this.
			return (strpos($v,$start) === 0)&&(substr($string, -1) == $end);
		}, ARRAY_FILTER_USE_KEY);
	}

	public function setURL($call,$param=null){
		$query = is_array($param)? http_build_query($param) : $param;
		switch($call)
		{
			case 'categories':
				$url = 'https://wordsapiv1.p.mashape.com/words/'.$query.'/inCategory';
				break;
			case 'typeOf':
				$url = 'https://wordsapiv1.p.mashape.com/words/'.$query.'/typeOf';
				break;
			case 'all':
				$url = 'https://wordsapiv1.p.mashape.com/words/';
			case 'words':
				$url = 'https://wordsapiv1.p.mashape.com/words/?'.$query; //This will be a string of queries
				break;
			case 'synonyms':
				$url = 'https://wordsapiv1.p.mashape.com/words/'.$query.'/synonyms';
				break;
			case 'word':
				$url = 'https://wordsapiv1.p.mashape.com/words/'.$query; //This will be a single string
				break;
			case 'domain':
				$url = 'https://api.godaddy.com/v1/domains/available?';
				break;
			case 'domains':
				$url = 'https://api.godaddy.com/v1/domains/available?checkType='.$query;
				break;
			case 'tlds':
				$url = 'https://api.godaddy.com/v1/domains/tlds';
				break;
		}
		return $url;
	}
}

?>