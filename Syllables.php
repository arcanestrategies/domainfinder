<?php

use \DaveChild\TextStatistics\Text as Text;
use \DaveChild\TextStatistics\Pluralise as Pluralise;

class Syllables{
	
    // Specific common exceptions that don't follow the rule set below are handled individually
    // array of problem words (with word as key, syllable count as value).
    // Common reasons we need to override some words:
    //   - Trailing 'e' is pronounced
    //   - Portmanteaus
    static public $arrProblemWords = array(
         'abalone'          => ['ab','a','lo','ne']
        ,'abare'            => ['ab','a','re']
        ,'abed'             => ['ab','ed']
        ,'abruzzese'        => ['ab','ruz','ze','se']
        ,'abbruzzese'       => ['ab','bru','ze','se']
        ,'aborigine'        => ['ab','o','rig','in','e']
        ,'acreage'          => ['ac','re','age']
        ,'adame'            => ['a','da','me']
        ,'adieu'            => ['a','dieu']
        ,'adobe'            => ['a','do','be']
        ,'anemone'          => ['a','ne','mo','ne']
        ,'apache'           => ['a','pach','e']
        ,'aphrodite'        => ['a','phro','di','te']
        ,'apostrophe'       => ['a','po','stro','phe']
        ,'ariadne'          => ['a','ri','ad','ne']
        ,'cafe'             => ['ca','fe']
        ,'calliope'         => ['cal','li','o','pe']
        ,'catastrophe'      => ['ca','ta','stro','phe']
        ,'chile'            => ['chi','le']
        ,'chloe'            => ['chlo','e']
        ,'circe'            => ['cir','ce']
        ,'coyote'           => ['coy','o','te']
        ,'epitome'          => ['e','pit','o','me']
        ,'forever'          => ['for','ev','er']
        ,'gethsemane'       => ['geth','se','man','e']
        ,'guacamole'        => ['gua','ca','mo','le']
        ,'hyperbole'        => ['hy','per','bo','le']
        ,'jesse'            => ['jes','se']
        ,'jukebox'          => ['juke','box']
        ,'karate'           => ['ka','ra','te']
        ,'machete'          => ['ma','che','te']
        ,'maybe'            => ['may','be']
        ,'people'           => ['peo','ple']
        ,'recipe'           => ['rec','i','pe']
        ,'sesame'           => ['se','sa','me']
        ,'shoreline'        => ['shore','line']
        ,'simile'           => ['sim','il','e']
        ,'syncope'          => ['sync','o','pe']
        ,'tamale'           => ['ta','ma','le']
        ,'yosemite'         => ['yo','sem','i','te']
        ,'daphne'           => ['da','phne']
        ,'eurydice'         => ['eur','y','dice']
        ,'euterpe'          => ['eu','ter','pe']
        ,'hermione'         => ['her','mi','o','ne']
        ,'penelope'         => ['pen','el','o','pe']
        ,'persephone'       => ['per','seph','o','ne']
        ,'phoebe'           => ['phoe','be']
        ,'zoe'              => ['zo','e']
    );

    // These syllables would be counted as two but should be one
    static public $arrSubSyllables = array(
         'cia(l|$)' // glacial, acacia
        ,'tia'
        ,'cius'
        ,'cious'
        ,'[^aeiou]giu'
        ,'[aeiouy][^aeiouy]ion'
        ,'iou'
        ,'sia$'
        ,'eous$'
        ,'[oa]gue$'
        ,'.[^aeiuoycgltdb]{2,}ed$'
        ,'.ely$'
        //,'[cg]h?ed?$'
        //,'rved?$'
        //,'[aeiouy][dt]es?$'
        //,'^[dr]e[aeiou][^aeiou]+$' // Sorts out deal, deign etc
        //,'[aeiouy]rse$' // Purse, hearse
        ,'^jua'
        //,'nne[ds]?$' // canadienne
        ,'uai' // acquainted
        ,'eau' // champeau
        //,'pagne[ds]?$' // champagne
        //,'[aeiouy][^aeiuoytdbcgrnzs]h?e[rsd]?$'
        // The following detects words ending with a soft e ending. Don't
        // mess with it unless you absolutely have to! The following
        // is a list of words you can use to test a new version of
        // this rule (add 'r', 's' and 'd' where possible to test
        // fully):
        //   - absolve
        //   - acquiesce
        //   - audience
        //   - ache
        //   - acquire
        //   - brunelle
        //   - byrne
        //   - canadienne
        //   - coughed
        //   - curved
        //   - champagne
        //   - designate
        //   - force
        //   - lace
        //   - late
        //   - lathe
        //   - make
        //   - relayed
        //   - scrounge
        //   - side
        //   - sideline
        //   - some
        //   - wide
        //   - taste
        ,'[aeiouy](b|c|ch|d|dg|f|g|gh|gn|k|l|ll|lv|m|mm|n|nc|ng|nn|p|r|rc|rn|rs|rv|s|sc|sk|sl|squ|ss|st|t|th|v|y|z)e$'
        // For soft e endings with a "d". Test words:
        //   - crunched
        //   - forced
        //   - hated
        //   - sided
        //   - sidelined
        //   - unexploded
        //   - unexplored
        //   - scrounged
        //   - squelched
        //   - forced
        ,'[aeiouy](b|c|ch|dg|f|g|gh|gn|k|l|lch|ll|lv|m|mm|n|nc|ng|nch|nn|p|r|rc|rn|rs|rv|s|sc|sk|sl|squ|ss|th|v|y|z)ed$'
        // For soft e endings with a "s". Test words:
        //   - absences
        //   - accomplices
        //   - acknowledges
        //   - advantages
        //   - byrnes
        //   - crunches
        //   - forces
        //   - scrounges
        //   - squelches
        ,'[aeiouy](b|ch|d|f|gh|gn|k|l|lch|ll|lv|m|mm|n|nch|nn|p|r|rn|rs|rv|s|sc|sk|sl|squ|ss|st|t|th|v|y)es$'
        ,'^busi$'
    );

    // These syllables would be counted as one but should be two
    static public $arrAddSyllables = array(
         '([^s]|^)ia'
        ,'riet'
        ,'dien' // audience
        ,'iu'
        ,'io'
        ,'eo($|[b-df-hj-np-tv-z])'
        ,'ii'
        ,'[ou]a$'
        ,'[aeiouym]bl$'
        ,'[aeiou]{3}'
        ,'[aeiou]y[aeiou]'
        ,'^mc'
        ,'ism$'
        ,'asm$'
        ,'thm$'
        ,'([^aeiouy])\1l$'
        ,'[^l]lien'
        ,'^coa[dglx].'
        ,'[^gq]ua[^auieo]'
        ,'dnt$'
        ,'uity$'
        ,'[^aeiouy]ie(r|st|t)$'
        ,'eings?$'
        ,'[aeiouy]sh?e[rsd]$'
        ,'iell'
        ,'dea$'
        ,'real' // real, cereal
        ,'[^aeiou]y[ae]' // bryan, byerley
        ,'gean$' // aegean
        ,'uen' // influence, affluence
    );

    // Single syllable prefixes and suffixes
    static public $arrAffix = array(
         '`^un`'
        ,'`^fore`'
        ,'`^ware`'
        ,'`^none?`'
        ,'`^out`'
        ,'`^post`'
        ,'`^sub`'
        ,'`^pre`'
        ,'`^pro`'
        ,'`^dis`'
        ,'`^side`'
        ,'`ly$`'
        ,'`less$`'
        ,'`some$`'
        ,'`ful$`'
        ,'`ers?$`'
        ,'`ness$`'
        ,'`cians?$`'
        ,'`ments?$`'
        ,'`ettes?$`'
        ,'`villes?$`'
        ,'`ships?$`'
        ,'`sides?$`'
        ,'`ports?$`'
        ,'`shires?$`'
        ,'`tion(ed)?$`'
    );

    // Double syllable prefixes and suffixes
    static public $arrDoubleAffix = array(
         '`^above`'
        ,'`^ant[ie]`'
        ,'`^counter`'
        ,'`^hyper`'
        ,'`^afore`'
        ,'`^agri`'
        ,'`^in[ft]ra`'
        ,'`^inter`'
        ,'`^over`'
        ,'`^semi`'
        ,'`^ultra`'
        ,'`^under`'
        ,'`^extra`'
        ,'`^dia`'
        ,'`^micro`'
        ,'`^mega`'
        ,'`^kilo`'
        ,'`^pico`'
        ,'`^nano`'
        ,'`^macro`'
        ,'`berry$`'
        ,'`woman$`'
        ,'`women$`'
    );

    // Triple syllable prefixes and suffixes
    static public $arrTripleAffix = array(
         '`ology$`'
        ,'`ologist$`'
        ,'`onomy$`'
        ,'`onomist$`'
    );
	
	static public function array_flatten($array) {
	  if (!is_array($array)) return FALSE;
	  $result = array();
	  foreach ($array as $key => $value) {
		if (is_array($value))
		  $result = array_merge($result,self::array_flatten($value));
		else $result[$key] = $value;
	  }
	  return $result;
	}
	
	static public function getSyllables($strWord, $strEncoding = '')
    {
        // Trim whitespace
        $strWord = trim($strWord);
		$results = [];

        if ($len = strlen(trim($strWord)) <= 2) {
			return [$strWord];
        }

        $strWord = preg_replace('`[^A-Za-z]`', '', $strWord);
        $strWord = strtolower($strWord);

        if (isset(self::$arrProblemWords[$strWord])) {
            return self::$arrProblemWords[$strWord];
        }
		
        //$singularWord = Pluralise::getSingular($strWord);
        //if ($singularWord != $strWord) {
            if (isset(self::$arrProblemWords[$strWord])) {
                return self::$arrProblemWords[$strWord];
            }
        //}
        // Remove prefixes and suffixes
		foreach(self::$arrAffix as $arg){
			preg_match($arg,$strWord,$single);
			array_merge($results,$single);
			$strWord = preg_replace($arg, '', $strWord, -1);
		}
		
		foreach(self::$arrDoubleAffix as $arg){
			preg_match($arg,$strWord,$double);
			foreach($double as $pair){
				$rr = preg_split('/([^aeiouy?]+[aeiouy?]+)/', $pair, 2, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
				$results = array_merge($results,$rr);
			}
			$strWord = preg_replace($arg, '', $strWord, -1);
		}
		
		foreach(self::$arrTripleAffix as $arg){
			preg_match($arg,$strWord,$triple);
			foreach($triple as $trip){
				$rr = preg_split('/([^aeiouy?]+[aeiouy?]+)/', $trip, 3, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);;
				$results = array_merge($results,$rr);
			}
			$strWord = preg_replace($arg, '', $strWord, -1);
		}
		
		$arrSubSyllableTemp = $arrAddSyllableTemp = [];
        /** Syllables which are reported as two but should be one, we regex it and return it as one hard-coded **/
		foreach (self::$arrSubSyllables as $key=>$strSyllable) {
            if(preg_match('`' . $strSyllable . '`', $strWord,$rz)){
				// REFACTOR 0: We should consider grabbing the previous consonant or following...
				$arrSubSyllableTemp[] = $rz;
				/*for($i=0;$i<count($rz);$i+=2){
					$second = !empty($rz[$i+1])? $rz[$i+1] : [];
					$rr = implode('',[$rz[$i],$second]);
					array_push($results,$rr);
				}*/
            }
        }
		$arrSubSyllableTemp = self::array_flatten($arrSubSyllableTemp);
		foreach ($arrSubSyllableTemp as $as){
			for($i=0;$i<count($arrSubSyllableTemp); $i++){
				if(false!=strpos($as,$arrSubSyllableTemp[$i])){
					unset($arrSubSyllableTemp[$i]);
				}
			}
		}
		$arrSubSyllableTemp = array_unique($arrSubSyllableTemp);
		foreach($arrSubSyllableTemp as $arg){
			$strWord = preg_replace('/'.$arg.'/u', '', $strWord, -1);
		}
		$results = array_merge($results,$arrSubSyllableTemp);
		
		/** Syllables which are reported as one but should be two, we regex it and return it as two per hard-coding **/
        foreach (self::$arrAddSyllables as $strSyllable){
            if(preg_match('`' . $strSyllable . '`', $strWord,$rz)){
				// REFACTOR 1: We should consider grabbing the previous consonant or following...
				$arrAddSyllableTemp[] = $rz;
				/*foreach($strSyllable as $pair){
					$rr = preg_split("/[aeiouy]/",$pair,2);
					$results = array_merge($results,$rr);
				}*/
            }
        }
		$arrAddSyllableTemp = self::array_flatten($arrAddSyllableTemp);
		foreach ($arrAddSyllableTemp as $as){
			for($i=0;$i<count($arrAddSyllableTemp); $i++){
				if(false!=strpos($as,$arrAddSyllableTemp[$i])){
					unset($arrAddSyllableTemp[$i]);
				}
			}
		}
		$arrAddSyllableTemp = array_unique($arrAddSyllableTemp);
		foreach($arrAddSyllableTemp as $arg){
			$strWord = preg_replace('/'.$arg.'/u', '', $strWord, -1);
		}

		$results = array_merge($results,$arrAddSyllableTemp);
		
		$arrWordParts = preg_split('/([^aeiouy?]+[aeiouy?]+)/', $strWord, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        foreach ($arrWordParts as $strWordPart) {
            if ($strWordPart <> '') {
				// REFACTOR 2: Here we should see if the syllable already belongs to another syllable
                $results[] = $strWordPart;
            }
        }
        return $results;
	}
	
	
}

?>