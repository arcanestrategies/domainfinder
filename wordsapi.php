<?php
require_once __DIR__ . '/controller.php';
require_once __DIR__ . '/words.php';
	
class WordsAPI extends Controller implements Settings
{
	public $wordsAPIQuery = wordsAPIParams;
	// WordAPI					REFACTOR: Can I remove all these?
	public $key;
	public $query;
	public $words;
	public $multiplier;
	public $limit=100;
	public $hasDetails;
	public $header = ['Accept: application/json', 'X-Mashape-Key: '.wordsAPIKey];

	public function __construct(){
		$this->hasDetails = self::setStatus(wordsAPIhasDetails,2);
	}
	
	public function getMatches($algorithm){
		$url = self::setURL('words',$algorithm->query);
		$results = json_decode(self::getAPI('GET',$url,$this->header));
		if(!empty($results)&&$results->results->total<strictCount){
			$numpages = ceil($results->results->total/$this->limit);
			$results = self::buildWords($numpages);
			return $results;
		}
		return false;
	}
	
	public function buildWords($pages){
		$now = time();
		$total = $words = [];
		for($i=1; $i<=$pages; $i++){
			$this->query['page'] = $i;							// We used to run setVars here but no sense in re-setting everything, just changing the page is fine.  We start with page 1
			$url = self::setURL('words',$this->query);			// To get the next page of results with the same query.
			$results = json_decode(self::getAPI('GET',$url,$this->header));
			if(!isset($results->results)){
				continue;
			}
			if(is_array($words)){
				$words = array_unique(array_merge($words,$results->results->data));	// The API, for whatever reason, sometimes acknowledges the "limit" and sometimes does not.  Pagination also doesn't work as expected... Page 1 with limit 100, basically could return thousands of results, since page 1... so in case there are any repeats, we'll make this unique.
			}
			if((time()-$now)>buildWordsTimeout){
				break;
			}
		}
		/*if(regexfilter === TRUE){								// We had this filter when regex wasn't working with WordsAPI but it might not be necessary anymore.  That's why it's set to false in settings
			foreach($words as $word){
				if(preg_match('/'.$this->letterPattern.'/',$word)){
					$total[] = $word;
				}
			}
		} else {
			$total = $words;
		}*/
		$total = $words;
		$this->query['page'] = 0;								// Now we reset back to 0, so the other calls can start at zero.
		return $total;
	}
	
	public function getWord($string,$parts=null){
		$flag = FALSE;
		$url = self::setURL('word',$string);
		$results = self::cache(WORDIR.$string.EXT,null,'GET',$url,$this->header);
		if(isset($results->word)){
			if(isset($results->results)){
				self::getDetails($results,$results,FALSE);	// Kind of pointless that we are doing this here.
				$flag = TRUE;
			}
		} else {
			$results = new stdClass();
			$results->word = $string;
			$flag = TRUE;
		}
		if(empty($results->parts)){
			if(!empty($parts)){
				$subparts = [];
				foreach($parts as $part){	// REFACTOR: Should the parent parts be broken down to the child parts?  I think that makes sense
					if(count($parts)>1&&$part!==$string){
						$rpart = self::getWord($part);
						if(!isset($rpart->word)){
							if(false !== ($foundfiles = self::findFile($part))){
								foreach($foundfiles as $found){
									$rpart = self::getWord($found);
									if($rpart!==false){
										break;
									}
								}
							}
						}
						if(isset($rpart->parts)){
							$subparts = array_merge($subparts,$rpart->parts);
						}
					}
				}
				$results->parts = !empty($subparts)? $subparts : $parts;
			} else {
				$results->parts = [$string];
			}
			if(!empty($results->parts)){
				$flag = TRUE;
			}
		}
		if(empty($results->pronunciationPattern)){
			$results->pronunciationPattern = self::setPronunciation($results,$parts);
			unset($results->pronunciation);
			if(!empty($results->pronunciationPattern)){	
				$flag = TRUE;
			}
		}
		if(empty($results->frequency)){
			$freq = [];
			if(!empty($results->parts)&&count($results->parts)>1){
				foreach($results->parts as $pp){
					$part = self::getWord($pp);
					$freq[] = (isset($part->frequency))? $part->frequency : 1;
				}
				$results->frequency = array_sum($freq)/count($freq);
				$flag = TRUE;
			}
		}
		if(empty($results->syllableparts)){
			self::setSyllableParts($string,$results);
			if(!empty($results->syllableparts)){
				$flag = TRUE;
			}
		}
		if(!isset($results->letters)){
			self::getProperties($results);
			if(!isset($results->letters)){
				$flag = TRUE;
			}
		}
		if($flag == TRUE){
			self::cache(WORDIR.$string.EXT,$results);
		}
		return $results;
	}
	
	public function getProperties(&$results){
		$string = $results->word;
		$results->startChar = $string[0];
		$results->endChar = substr($string,-1);
		$speechSounds = self::speechSound($string);
		$results->consecVowels = $speechSounds->vows = isset($results->consecVowels)? $results->consecVowels : $speechSounds->vows;
		$results->consecConsenants = $speechSounds->cons = isset($results->consecConsenants)? $results->consecConsenants : $speechSounds->cons;
		$results->startPos = $speechSounds->starter = isset($results->startWordPos)? $results->startWordPos : $speechSounds->starter;
		$results->endPos = $speechSounds->ender = isset($results->endWordPos)? $results->endWordPos : $speechSounds->ender;
		$results->letters = strlen($string);
		$results->startPos = $speechSounds->starter;
		$results->endPos = $speechSounds->ender;
		$results->posSwitch = ((isset($results->startPos)&&isset($results->endPos))&&$results->startPos==$results->endPos)? 'FALSE' : 'TRUE'; // PosSwitch TRUE means it does switch
	}
	
	/**
		getDetails - loop to set hasDetails values
	**/
	public function getDetails(&$results,&$object,$settings_flag=FALSE){
		if(isset($results)&&!empty($results)&&isset($results->results)&&!empty($results->results)){
			foreach($results->results as $result){
				foreach($this->hasDetails as $detailType=>$value){
					if(isset($result->$detailType)&&$value>=2){
						if(is_array($result->$detailType)){
							foreach($result->$detailType as $v){
								$results->$detailType[] = $v;
								if($settings_flag==TRUE){
									$object[$detailType][] = $v;
								}
							}
						} else {
							$results->$detailType[] = $result->$detailType;
							if($settings_flag==TRUE){
								$object[$detailType][] = $result->$detailType;
							}
						}
					}
				}
			}
			unset($results->results);
		}
		foreach($this->hasDetails as $deet=>$status){
			if($status<2||empty($results->$deet)){
				unset($results->$deet);
			} else {
				if($settings_flag==TRUE){
					$object[$deet] = $results->$deet;
				} else {
					$object->$deet = $results->$deet;
				}
			}
		}
	}
	
	public function setPronunciation($object,$parts=null){
		$results = [];
		if(!empty($object->pronunciation)){
			$arg = $object->pronunciation;
			$rr = '';
			if(is_object($arg)||is_array($arg)){
				if(isset($arg->all)){
					$arg = $arg->all;					
				}
				if(is_object($arg)||is_array($arg)){
					foreach($arg as $aa){
						$rr .= self::setPronunciation($aa);
					}
					$arg = $rr;
				}
			}
			if(standardpronunciations==false){
				preg_match_all('/[\W]+/', $arg, $match);	// isolates non-standard sounding characters... these are the accents that give us hard and soft character pronunciations.
				if(isset($match[0])&&!empty($match[0])){
					foreach($match[0] as $item){
						$results[] = $item;
					}
				} else {
					preg_match_all('/[aeiouy]+/', $arg, $match);	// if we get no results we'll just pull the vowels
					if(isset($match[0])&&!empty($match[0])){
						foreach($match[0] as $item){
							$results[] = $item;
						}
					}
				}
			} else {
				$results = self::str_split_unicode($arg);
			}
		} else {
			if(!empty($parts)){
				foreach($parts as $part){
					if($part!==$object->word){
						$pr = self::getWord($part);
						$results = array_merge($results,$pr->pronunciationPattern);
					}
				}
			}
		}
		return $results;
	}
	
	/**
		In the event that the API does not return syllables, we gather syllables from other available properties like word parts and vendor functions
		In this case, we value parts higher than the vendor function. REFACTOR: Should we reverse that?
	**/
	public function setSyllableParts($word,&$results){
		if(!empty($results->syllables)&&!is_numeric($results->syllables)){
			if(self::is_assoc($results->syllables)==false){
				$list = $results->syllables;
				$results->syllableparts = $list;
			} else {
				$results->syllableparts = $results->syllables->list;
			}
		}
		if(empty($results->syllableparts)){
			if(isset($results->parts)&&count($results->parts)>1){
				$results->syllableparts = [];
				foreach($results->parts as $part){
					if($part!==$word){
						$temp = self::getWord($part);
						self::setSyllableParts($part,$temp);
						$results->syllableparts = array_merge($results->syllableparts,$temp->syllableparts);
					}
				}
			}
		}
		if(empty($results->parts)){
			$results->parts = [$word];
		}
		if(empty($results->syllableparts)){
			if(null !== ($syllables = Syllables::getSyllables($word))){
				$results->syllableparts = $syllables;
			} else {
				$results->syllableparts = $results->palts->parts;
			}
		}
		self::explodeString($results->syllableparts);
		$results->syllables = count($results->syllableparts);
		return true;
	}
	
	/** setVars
		Sets WordsAPI query parameters from the evaluated brand parameters.  Also sets some hard-coded regexes.	PronunciationPattern can severely limit results, so we only use it if it's actually a highly weighted pattern, which it often is not.		"hasDetails" is only 2/3 effective.  Sometimes good words can be filtered out simply because the API has incomplete data on the word's definitions and so forth.
	**/
	public function setVars(&$query,$properties){
		$query['limit'] = $this->limit;
		$this->query = $query;
		$this->properties = $properties;
	}
		
}


?>
