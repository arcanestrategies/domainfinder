<?php
require_once __DIR__ . '/settings.php';
require_once __DIR__ . '/words.php';
require_once __DIR__ . '/wordsapi.php';

/**
	1) Algorithm accepts 3 parameters: The limit of results per API call, page of results to display, extension for domain like .com or .net
**/

class Algorithm extends Controller
{
	public $brands;
	public $highscore;
	public $percentile;
	public $multiplier;
	public $properties;
	public $wordsProperties;
	/** Default algorithm settigns.  These will be overridden by the algorithm **/
	public $lettersMin = lettersMin;
	public $lettersMax = lettersMax;
	public $syllablesMin = syllablesMin;
	public $syllablesMax = syllablesMax; // Per word syllables. With 2 words, that means max 4.
	public $consecConsenantsMax = consecConsenantsMax;
	public $consecVowelsMax = consecVowelsMax;
	public $wordCountMin = wordCountMin;
	public $wordCountMax = wordCountMax;
	public $frequencyMin = frequencyMin;
	public $frequencyMax = frequencyMax;
	public $allParts = parts;
	public $prefixes = prefixes;
	public $suffixes = suffixes;
	public $filterout = bleep;
	public $args = ['min','max','mode','wmode','weach','wunique'];	// The minimum value of array, maximum value of array, weight/density of mode (count/total count), weight/density of each (returns array), count of unique values.
	/** regexes **/
	public $letterPattern;
	public $pronunciationPattern;
	/** end regexes **/
	
	public function __construct($multiplier=multiplier,$percentile=percentile,$duration=duration){
		$this->settings['pronunciationPattern'] = $this->settings['parts'] = $this->settings['syllableparts'] = $this->domains = $this->settings = $this->APIQuery = [];
		$this->multiplier = $multiplier;
		$this->percentile = $percentile;		// This var is the setting to determine what top percentile we want to collect.  Rather than taking the top percentile, though, we're going to take a percentage from highest score (ie. % deviation below highest)
		$this->expiry = 60*60*24*$duration;		// Cache for X days
		$this->APIQuery = self::setStatus(wordsAPIParams);
		$properties = array_merge($this->APIQuery,self::setStatus(wordProperties),self::setStatus(wordshasDetails,2)); // wordcount is total letter count of full word. partPosSwitch means if a word part start and end is either the same part of speech or different.
		$this->properties = $properties = array_filter($properties);	// filters out anything which is false/0
		$this->words = new Words();
		$this->api = new WordsAPI();
		$this->hasDetails = $this->api->hasDetails;
		$this->wordsProperties = array_merge($this->properties,$this->hasDetails);
		$this->words->properties = $this->wordsProperties;
		return true;
	}
	
	public function run($brands=null,$keyword=null){
		$this->brands = $brands;
		if(null==$this->brands||empty($this->brands)){			// REFACTOR:  Use a flat array, use library to determine root words from each string, then we could easily count consenants and vowels for a word, wordcount, syllable count, etc... and it'd be more scalable
			$this->brands = brands;				
		}
		self::brandProperties($this->brands);					// Evaluates Brands
		self::weightsRanges($this->properties,$this->args);		// Writes weight and range parameters
		self::weightsRanges($this->hasDetails,$this->args);		// Writes weights for definition wordsself::weightsRanges($this->hasDetails,$this->args);		// Writes weights for definition words
		$results = self::runResults('WordsAPI');
		if(strictCount < count($results) || false==$results){
			$results = self::runResults('Words');
		}
		if($results!==false&&!empty($results)){
			$results = self::rank($results,true);							// (1) Ranks returned word-set relevancy to brand words.
			$this->highscore = current(array_keys($results));				// (2) Sets the highest score, so we can evaluate only the top percentile;
			$this->allParts = $results = self::rank($this->allParts,false);	// (3) Extracts word parts from step 1 data, re-combines with original data-set and re-ranks. 
			$results = self::unsetLowScores($this->allParts,false);			// (4) Unsets the low scores, so we're only evaluating top X relevant words.
			$this->prefixes = array_merge($this->prefixes,$results);		// (5) Combines the filtered words with common prefixes, to be added during permutation
			$this->suffixes = array_merge($this->suffixes,$results);		// (6) Combines the filtered worsd with common suffixes, to be added during permutation
			// ^ With this change above, since we haven't re-ranked these, we are slightly spoiling the data by not unsetting bad ranking prefixes and suffixes.
			//$this->prefixes = self::rank($this->prefixes,false);			// (5) Ranks prefixes
			//$this->prefixes = self::unsetLowScores($this->prefixes,true);	// (6) Un-sets low ranking prefixes
			//$this->suffixes = self::rank($this->suffixes,false);			// (7) Ranks suffixes
			//$this->suffixes = self::unsetLowScores($this->suffixes,true);	// (8) Un-sets common suffixes
			$results = self::permute($results);								// (9) Merges the ordered word parts from 4 with a set of combined words from 4+[6+8] and re-ranks and sorts the full set.  This is the domain array.
			$results = self::unsetLowScores($results,false);				// (10) Unsets low scores from combined word parts. 
			$this->domains = $results;
			return $results;
		}
		return false;
	}
	
	/**
		permute 
		builds 1 and 2 word pairings of our weighted words.
		REFACTOR 0:  Eventually, we should have all the wordcount Min and Max values evaluated in a loop, switching between the two, using strpos kind of like setVars but in reverse. This way nothing is hard-coded.  Same with WordFilter. Should we consider permutations of syllables instead of permutations of words?
	**/
	public function permute($items) {
		$domains = [];
		foreach ($items as $index=>$first) {
			if (0==preg_match('/'.$this->letterPattern.'/',$first)||strlen($first)<$this->wordCountMin||$this->wordCountMax>strlen($first)){
				unset($items[$index]);
			}
			// REFACTOR: Use the prefixes and suffixes library from syllables and append/prepend those
			foreach($this->prefixes as $prefix){
				if($first==$prefix){
					continue;
				}
				$string = $prefix.$first;
				if (1==preg_match('/'.$this->letterPattern.'/',$string)&&strlen($prefix)<=$this->lettersMax&&strlen($prefix)>=$this->lettersMin&&!in_array($string,$items)&&$this->wordCountMin<=strlen($string)&&strlen($string)<=$this->wordCountMax){					
					$domains[] = ['word'=>$string,'parts'=>[$prefix,$first]];
				}
			}
			foreach($this->suffixes as $suffix){
				if($first==$suffix){
					continue;
				}
				$string = $first.$suffix;
				if (1==preg_match('/'.$this->letterPattern.'/',$string)&&strlen($suffix)<=$this->lettersMax&&strlen($suffix)>=$this->lettersMin&&!in_array($string,$items)&&$this->wordCountMin<=strlen($string)&&strlen($string)<=$this->wordCountMax){
					$domains[] = ['word'=>$string,'parts'=>[$first,$suffix]];
				}
			}
		}
		if(!empty($domains)){
			$domains = self::rank($domains);	// Get the "Word" score of the existing combined part scores
		}
		$domains = $items+$domains;
		krsort($domains);
		return $domains;
	}
	
	/**	weightsRanges
		Builds Property Value Ranges and Weights && Property Weights... 0 is disabled, 1 is enabled, 2 is enabled with mins and maxes
			(1) Runs through each property key (which are the actual properties... their values are weights).  Using this, it sets the object's min, max, and weight values of each property.
			(2) Uses the uniqueness and mode of each property, to build weights for each property.  returns the weights.
		Accepts optional 3rd var of "TRUE/FALSE" to enable/disable the min/max property types
	**/
	
	public function weightsRanges($properties,$args){
		foreach($properties as $property=>$value){
			foreach($args as $param){
				if(in_array($param,['min','max'])&&$value>=2){
					continue;
				}
				$var = $property.ucfirst($param);
				if(isset($this->settings[$property])&&count($this->settings[$property])>0){
					$variable = self::evaluate($this->settings[$property],$param);
					if($variable !== false){
						$this->$var = $variable;
					}
				}
			}
			$wmode = $property.ucfirst('wmode');
			$wunique = $property.ucfirst('wunique');
			// REFACTOR:  We are taking the greater of either the mode or its uniqueness
			if(isset($this->$wmode)&&isset($this->$wunique)){
				$this->properties[$property] = max($this->$wmode,$this->$wunique);
			}
		}
		arsort($this->properties);
		return true;
	}
	
	/** evaluate
		returns the value that matches an operator/evaluator against an input array.  "min", for instance, requests the minimum value of array "$input"
	**/
	
	public function evaluate($input, $evaluator, &$array=NULL){
		if(is_object($input)){
			$input = (array)$input;
		}
		switch($evaluator){
				case "min":
					if(!empty($input)){
						return min($input);
					} else {
						return false;
					}
				case "max":
					if(!empty($input)){
						return max($input);
					} else {
						return false;
					}
				case "mode":
					$array = [];
					$all = self::evaluate($input, 'weach', $array);	// returns mode and revised array (if floats).
					if(!empty($all)){
						$array = $all;									// passes weache's weighted array back to wmode
						$mode = array_keys($all, max($all));			// get the key of maximum value
						return $mode[0];								// returns the 1st match
					} else {
						return false;
					}
				case "wmode":
					$array = [];
					$mode = self::evaluate($input, 'mode', $array);	// returns mode and revised array.
					if(!empty($mode)&&isset($array[$mode])){
						return $array[$mode]; 							// get density of mode
					} else {
						return false;
					}
				case "weach":											// Should we do this as the default, too?
					$results = [];
					$array = $input;
					if(self::is_multi_array($array)){
						$array = self::array_flatten($array);
					}
					/* In the case that the array contains a float value, which is unlikely to produde a mode, we'll round to a whole number and we'll evaluate against whole numbers later on, as well. */
					foreach($input as $key=>$item){
						if(is_float($item)){
							$array[$key] = intval($item);
						}
					}
					$total = count($array);
					if($total>0){
						$c = array_count_values($array); 				// get count of each unique value into an array format (ie. ["Value"=>"count","Value 2"=>"count 2"]
						foreach($c as $key=>$count){						
							$results[$key] = $count/$total;
						}
						return $results;
					} else {
						return false;
					}
				case "wunique":
					$array = $input;
					/* In the case that the array contains a float value, which is unlikely to produde a mode, we'll round to a whole number and we'll evaluate against whole numbers later on, as well. */
					//$array = self::evaluate($input, 'weach', $array);	// get the revised array by reference.
					if(self::is_multi_array($array)){
						$array = self::array_flatten($array);
					}
					$total = count($array);
					if($total>0){
						$unique = count(array_unique($array));
						return ($total-$unique)/$total;  					// gets weight of uniqueness.  Lower unique values means higher weight.
					} else {
						return false;
					}
		}
	}
	
	/**
		getProperties
		gets the properties of a given string, using WordsAPI.  Accepts an optional 2nd param of $parts
	**/
	public function getProperties($string,$parts=null){
		return self::getWord($string,$parts);
	}
	
	/**
		getWeight
		gets the weight of all properties for a given object of property values.
		returns a single array of property=>weight for given object.
	**/
	public function getWeight($object){
		$results = $temp = [];
		foreach($object as $property=>$value){
			$prop = $property.'Weach';					// we want the "Weach" key of a property because it has the weights of all possible values, where the value is actually the key.
			if(isset($this->$prop)){
				if(!is_array($value)&&!is_object($value)){
					if(is_float($value)){
						$value=intval($value);
					}
					$results[$property] = (array_key_exists($value,$this->$prop))? $this->$prop[$value] : 0;		// We're actually looking for the keys, not the values; the values are weights.
				} else {
					$results[$property] = 0;
					foreach($value as $var){
						if(array_key_exists($var,$this->$prop)){	//key,array
							$results[$property] = $results[$property]+$this->$prop[$var];
						}
					}
				}
			}
		}
		return $results;
	}
	
	/**
		weigh
		weighs a string for a single combined score of all property values
		second optional property are the parts which make up the word, if applicable.  where not applicable, it is null.
	**/
	public function weigh($object){
		$score = 0;
		if($object->letters > $this->wordCountMax ||
			$this->lettersMin > $object->letters ||
			$object->syllables > $this->wordSyllablesMax ||
			$this->syllablesMin > $object->syllables){
				return 0;
		}
		$weights = self::getWeight($object);
		foreach($this->properties as $prop=>$weight){
			if(isset($weights[$prop])){
				$score += $weights[$prop]*$weight;
			}
		}
		return $score;
	}
	
	
	/**
		rank
		ranks an array by score
	**/
	public function rank($array,$flag=false){
		$results = $temp = [];
		$i = 0;
		foreach($array as $key=>$word){
			$this->words->explodeWord($word);
			$parts = null;
			if(is_array($word)){
				$parts = $word['parts'];
				$word = $word['word'];
				if($word==null||in_array($word,$this->filterout)||in_array($word,$results)){
					continue;
				}
				$word = self::getProperties($word,$parts);
			}
			if(is_string($word)){
				if(in_array($word,$this->filterout)||in_array($word,$results)){
					continue;
				}
				$word = self::getProperties($word);
			}
			$w = $word->word;
			if(1 === preg_match('~[0-9]~', $w)||in_array($w,$this->filterout)||in_array($w,$results)){
				continue;
			}
			if(preg_match('/[^a-zA-Z\d]/', $w)){
				$w = preg_replace('/[^a-zA-Z\d]/', '-', $w);
				$ww = str_replace('-', '', $w);
				if(!in_array($ww,$results)){
					$pp = explode('-',$w);
					$word = self::getProperties($ww,$pp);
				}
			}
			if(includeparts==true&&$flag==true&&!empty($this->allParts)){
				if(is_array($word->parts)&&!empty($word->parts)){
					$this->allParts = array_merge($this->allParts,$word->parts);
				}
			}
			$score = round(self::weigh($word)*$this->multiplier);			// We can't use decimals in sorting.
			if($score==0){
				continue;
			}
			if(is_array($word)&&is_numeric($key)&&$key>($this->multiplier/10)){	// If a word has an existing score (as a part), it would be greater than the multiplier minus 1 decimal.  We'll add the "part" score to the "word" score, for a final score.
				$score = $score+$key;
			}
			if(isset($results[$score])){
				$score += $i;													// This is just to avoid conflicts
			}
			$results[$score] = $w;
			$i++;
		}
		krsort($results);
		return $results;
	}
	
	// This next block is strictly used to return the count of all results, for pagination purposes
	// Sets WordsAPI parameters and hard-coded params based on brand values.
	public function runResults($source='Words'){
		self::setVars();
		$words = new $source(platform);
		$words->setVars($this->query,$this->properties);
		$results = $words->getMatches($this);
		return $results;
	}
	
	public function getWord($string,$parts=NULL){
		$result = $this->words->getWord($string);
		if($result==false){
			$result = $this->api->getWord($string,$parts);
			//shell_exec("php import.php ".$string." local");
		} else {
			unset($result->id);
		}
		return $result;
	}
	
	public function setVars(){
		$regex = '';
		$this->letterPattern = '^(?:(.)(';
		/** (1) The letter pattern is set as an object property here but will be grabbed from the APIQuery loop, below **/										
		$this->letterPattern.= '(?!\1{2,})';														// No triple characters (matches twice past the first)		
		$this->letterPattern.= '(?!([b-df-hj-np-tv-z]{'.($this->consecConsenantsMax+1).',}))';		// Cannot have more than the maximum number of consecutive consenants
		$this->letterPattern.= '(?!([aeiouy]{'.($this->consecVowelsMax+1).',}))';					// Cannot have more than the maximum number of consecutive vowels
		if(doublecharacterpairs===false){
			$this->letterPattern.= '(?=([a-zA-Z])\5{1,}(?!([a-zA-Z])\6{1,}))';
			// Cannot have 2 consecutive pairs of double characters. Not sure this actually works.
		}
		$this->letterPattern.= '))*$';
		/** (3) This loop will set query values for enabled APIQuery properties. These are strict outer boundaries (ie. min/max). **/
		foreach($this->APIQuery as $property=>$value){
			if($property=='hasDetails'){
				$cats = '';
				foreach($this->hasDetails as $search=>$v){
					if($v!==false){
						$cats .= $search.',';
					}
				}
				$this->query[$property] = rtrim($cats,',');
			} else {
				if(isset($this->$property)&&$value>0){
					$this->query[$property] = $this->$property;
				}
				foreach(['min','max'] as $arg){
					if($value==1){
						$p = $property.ucfirst($arg);
						if(isset($this->$p)&&is_numeric($this->$p)){
							$this->query[$p] = $this->$p;
						}
					}
				}
			}
		}
		/** (4) Pronunciation pattern is specifically singled out becuase it is a viable Mashape Words API call.  We will have to pick up MySQL queries in a separate step **/
		if(isset($this->pronunciationPatternWeach)&&isset($this->pronunciationPatternWmode)&&($this->pronunciationPatternWmode >= strictThreshold)){
			$i = 0;
			foreach($this->pronunciationPatternWeach as $pattern=>$weight){
					if($weight == $this->pronunciationPatternWmode || $weight >= $this->pronunciationPatternWmode/strictThreshold){			// grab any pattern that's the mode or weighted within a specific percentage... (ie. if weighted at 0.33, it is 33% of the total
						if(false==strpos($regex,$pattern)){
							$regex .= $pattern;
							$i++;
						}
					}
			}
			if($i>0){ $this->query['pronunciationPattern'] = '.*['.$regex.']$'; }
		}
		return true;
	}
	
	/** unsetLowScores
		unsets the low scores so that we're only evaluating the top scores.  This sets a percentage variance relative to the score.  If the total count is less than Y, though, it ignores the new set and defaults to the top X percentage
		This is based on the highscore var (at object level) and the percentile var (at object level)
	**/
	public function unsetLowScores($array,$flag=false){
		if(null!==limiter&&count($array)<=limiter&&$flag==false){
			return $array;
		}
		if(!isset($this->highscore)||!isset($this->percentile)){
			throw new \Exception('You are missing the percentile or highscore... you need to set those before running this');
		}
		$cut = $this->highscore*((100-$this->percentile)/100);
		$i = 0;
		foreach($array as $score=>$var){
			if($score < $cut){
				break;
			}
			$i++;
		}
		$array_divergence = array_slice($array,0,$i,true);
		if($flag==true||(null!==limiter&&count($array_divergence)>=limiter)){
			return $array_divergence;
		}
		$total = count($array);
		$pct = round($total*(($this->percentile/100)),PHP_ROUND_HALF_UP);
		if($pct<1){
			$pct = 1;
		}
		$array_percent = array_slice($array,0,$pct,true);
		if($flag==true||(null!==limiter&&count($array_percent)>=limiter)){
			return $array_percent;
		}
		$array = array_slice($array,0,limiter,true);		
		return $array;
	}
	
	/** 
		brandProperties
		Uses WordsAPI to get values of our given properties.
	**/
	public function brandProperties($brands,$type='word'){
		foreach($brands as $i=>$string){
			$parts = is_array($string)? $string : null;
			$string = is_array($string)? implode('',$string) : $string;
			$string = strtolower($string);
			$results = self::getProperties($string,$parts);	// This will get the properties of a brand by its combined parts
			if($results==false){
				continue;
			}
			$speechSounds = self::speechSound($string);
			$this->api->getDetails($results,$this->settings,TRUE);
			if($parts==NULL){	// If parts is null, this is the smallest we can query.
				if(!empty($results->syllables)){
					$this->settings['syllables'][] = $results->syllables;
				}
				if(!empty($results->pronunciationPattern)){
					$this->settings['pronunciationPattern'][] = $results->pronunciationPattern;
				}
				if(!empty($results->frequency)){
					$this->settings['frequency'][] = $results->frequency;
				}
				$this->settings['parts'][] = $string;
				if(!empty($results->syllableparts)){
					$this->settings['syllableparts'] = array_merge($this->settings['syllableparts'],$results->syllableparts);
				}
				$this->settings['letters'][] = !empty($results->letters)? $results->letters : strlen($string);
			}
			$this->settings['startChar'][] = $string[0];
			$this->settings['endChar'][] = substr($string,-1);
			$this->settings['startPos'][] = $speechSounds->starter;
			$this->settings['endPos'][] = $speechSounds->ender;				// 'end'.ucfirst($type).'Pos'
			$this->settings['posSwitch'][] = ($speechSounds->starter==$speechSounds->ender)? 'FALSE' : 'TRUE'; // PosSwitch TRUE means it does switch
			$this->settings['consecVowels'][] = $speechSounds->vows;	// REFACTOR: We could do a part and word for this but we're only using for max anyway
			$this->settings['consecConsenants'][] = $speechSounds->cons;	// REFACTOR: We could do a part and word for this but we're only using for max anyway
			if($type=='word'){
				$this->settings['words'][] = $results->word = $string;
				$this->settings['wordSyllables'][] = $results->syllables;
				$this->settings['wordCount'][] = $results->letters;
			}
			if($parts!==null){
				self::brandProperties($parts,'part');
			}
		}
		return true;
	}
	
}

?>
