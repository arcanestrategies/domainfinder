<?php
/**
*	Words API
*	DOCS: https://market.mashape.com/wordsapi/wordsapi
*	KEY: ovM95NTp9XmshVyL8gVCkh8utlMwp1r9CaxjsnTuJIiu9vgfTP
*	(1) Analyze top brands for patterns
*	(2) Get all words from dictionary that match those patterns
*	(3) Make a list of all words from #2 and all 2 word permutations from #2 with a *.com extension
*
*	Godaddy API
*	DOCS: https://developer.godaddy.com/doc
*	KEY: 9ZpU9FFR63i_7Ume4MVnnwoeF85bK5F2Fb:7Umi8uNJiB2MHcgB8MgWNM;
*	(5) Query all words from 4 against Godaddy
*	(6) filter down to all words where price is less than $1,000.
*	(7) If count is lower than 100, repeat steps 2-6 without specific category
**/

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/godaddyapi.php';
require __DIR__ . '/words.php';
require __DIR__ . '/algorithm.php';

if(Controller::require_auth()){

	/**	Retrieves possible Godaddy TLD's for the dropdown **/
	$godaddy = new GodaddyAPI();
	$tlds = $godaddy->getTlds();
	echo json_encode($tlds);
	
}
?>