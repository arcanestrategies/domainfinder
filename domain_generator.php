<?php
/**
*	Words API
*	DOCS: https://market.mashape.com/wordsapi/wordsapi
*	KEY: ovM95NTp9XmshVyL8gVCkh8utlMwp1r9CaxjsnTuJIiu9vgfTP
*	(1) Analyze top brands for patterns
*	(2) Get all words from dictionary that match those patterns
*	(3) Make a list of all words from #2 and all 2 word permutations from #2 with a *.com extension
*
*	Godaddy API
*	DOCS: https://developer.godaddy.com/doc
*	KEY: 9ZpU9FFR63i_7Ume4MVnnwoeF85bK5F2Fb:7Umi8uNJiB2MHcgB8MgWNM;
*	(5) Query all words from 4 against Godaddy
*	(6) filter down to all words where price is less than $1,000.
*	(7) If count is lower than 100, repeat steps 2-6 without specific category
**/

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/godaddyapi.php';
require __DIR__ . '/words.php';
require __DIR__ . '/algorithm.php';

if(Controller::require_auth()){

	$data = json_decode(file_get_contents("php://input"));
	$max = isset($data->max)? $data->max : 40;								// Maximum results per call
	$limit = isset($data->limit)? $data->limit : null;						// Maximum results per call
	$pricelimit = isset($data->price)? str_replace('$','',$data->price) : 13;	// Price limit (10099000 = 1,009.90)
	$top = isset($data->topranking)? $data->topranking : limiter;				// Return the top X results
	$extension = isset($data->extension)? $data->extension : '.com';			// With the followin extension
	$keyword = isset($data->keyword)? $data->keyword : null;					// Category or keyword
	$brands = isset($data->brands)? $data->brands : null;
	/** Instantiates WordsAPI object which does the following:
		(1) Analyzes popular brands (ie. #chars, #consecutive vowels, #consecutive consonants, pronunciation similarities, frequency in daily use, etc.)
		(2) Uses those params to pass to WordsAPI, to return values of similar properties.
		(3) Then creates 2-word permutations of those returned values that adhere to the same properties, as *.com domains.
	**/
	$algorithm = new Algorithm();
	$words = $algorithm->run($brands,$keyword);
	/**	(4) Passes those values to the Godaddy API to see if they are available and reasonably priced. **/
	if($words!=false){
		$domains = new GodaddyAPI($words,$max,$pricelimit,$top,$extension);
		$name_count = count($domains->results);
		$domain_count = count($domains->domains);
		if(null!==$limit){
			$domains->results = array_slice($domains->results,0,$limit,TRUE);
			$domains->domains = array_slice($domains->domains,0,$limit,TRUE);
		}
		echo json_encode(['results'=>$domains->results,'domains'=>$domains->domains,'count'=>['names'=>$name_count,'domains'=>$domain_count]]);
	} else {
		echo json_encode(['error'=>'words returned false']);
	}
	
}
?>
